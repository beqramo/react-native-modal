import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import {
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {
  PanGestureHandler,
  NativeViewGestureHandler,
} from 'react-native-gesture-handler';
import Animated, {
  useSharedValue,
  withTiming,
  useAnimatedStyle,
  interpolate,
  runOnJS,
  withSpring,
  useAnimatedGestureHandler,
  Extrapolate,
  useAnimatedScrollHandler,
} from 'react-native-reanimated';
import ModalItem from './ModalItem';

const {height, width} = Dimensions.get('window');

const options = [
  {
    text: 'first',
    id: 234,
  },
  {
    text: 'second',
    id: 23,
  },
];
/**
 *  sorry for the layout, and also code structure, I usually write in TS and with my code structure,
 *  but it was a little task and for the sake of time I decided to just make it as simple as possible,
 *  my main motivation was to use new react-native reanimated library with worklet and I think it is awesome
 *  BTW, sorry for the video, I forgot to record it and also if you want I can show you my experience pretty easily with a just 10-minute call,
 *  and also I will show you if you want my real word project with its codes and structure.
 *  also, I will tell you that it took longer then I expected because it was the first touch on the new version
 *
 *  as I know other libraries integration doesn't use scrollView they are using just e pan responders and saving a translateY number,
 *  I could do that way but this isn't performant way on a big data.
 *
 *
 *  thanks. I tried my best to make it bug-free as possible, again, I have limited time to do it so, sorry.
 *
 *  P.S. Have a nice day :).
 */

// I'm not using propTypes because I hate it in pure js, TS forever :)
const Modal = ({isActive, onModalToggle, activeId, setActiveId, maxHeight}) => {
  const [modalActive, setModalActive] = useState(0);
  const animation = useSharedValue(0);
  const touchY = useSharedValue(1);
  const [panEnabled, setPanEnabled] = useState(true);
  const pan = useRef();
  const scrollView = useRef();

  const onCancel = useCallback(() => {
    onModalToggle(0);
    animation.value = withTiming(0, {duration: 300}, (isFinished) => {
      isFinished && runOnJS(setModalActive)(0);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [animation.value, onModalToggle, setModalActive]);
  const onSelectValue = useCallback(
    (id) => {
      setActiveId(id);
      onCancel();
    },
    [setActiveId, onCancel],
  );
  const scroll = useAnimatedScrollHandler(
    {
      onMomentumEnd: (e) => {
        if (e.contentOffset.y <= 0) {
          runOnJS(setPanEnabled)(true);
        } else {
          runOnJS(setPanEnabled)(false);
        }
        if (e.contentOffset.y <= -20) {
          runOnJS(onCancel)();
        }
      },
      onScroll: (e) => {
        if (e.contentOffset.y <= 0) {
          runOnJS(setPanEnabled)(true);
        } else {
          runOnJS(setPanEnabled)(false);
        }
        if (e.contentOffset.y < 0) {
          if (animation.value === 0 || e.contentOffset.y < -20) {
            runOnJS(onCancel)();
          } else {
            animation.value = interpolate(
              Math.abs(e.contentOffset.y),
              [0, 50],
              [1, 0],
              Extrapolate.CLAMP,
            );
          }
        }
      },
    },
    [animation.value, onCancel, setPanEnabled],
  );

  const handler = useAnimatedGestureHandler(
    {
      onStart: (_, ctx) => {
        ctx.startY = touchY.value;
      },
      onActive: (event, ctx) => {
        animation.value = interpolate(
          event.translationY,
          [0, 100],
          [1, 0],
          Extrapolate.CLAMP,
        );
      },
      onEnd: (a) => {
        runOnJS(setPanEnabled)(false);
        if (a.translationY > 20) {
          runOnJS(onCancel)();
        } else {
          animation.value = withSpring(1);
        }
      },
    },
    [setPanEnabled, onCancel, animation.value],
  );

  useEffect(() => {
    if (isActive) {
      setModalActive(1);
      animation.value = withTiming(1, {duration: 300});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isActive, animation.value, setModalActive]);

  const background = useAnimatedStyle(() => {
    return {
      opacity: interpolate(animation.value, [0, 1], [0, 0.6]),
    };
  }, [animation.value]);

  const mainView = useAnimatedStyle(() => {
    return {
      transform: [
        {translateY: interpolate(animation.value, [0, 1], [maxHeight, 0])},
      ],
    };
  }, [animation.value, maxHeight]);

  return useMemo(
    () =>
      modalActive ? (
        <PanGestureHandler
          onGestureEvent={handler}
          waitFor={scrollView}
          ref={pan}>
          <Animated.View style={styles.mainContainer}>
            <Animated.View style={[styles.container, background]}>
              <TouchableOpacity
                onPress={onCancel}
                style={[styles.pressable]}
                activeOpacity={1}
              />
            </Animated.View>
            <Animated.View style={[styles.mainView, mainView, {maxHeight}]}>
              <NativeViewGestureHandler
                // android native responders kinda need to be purified, when there is a scroll it kinda become irresponsive
                // sadly what I hate most about react-native-reanimated and react-native-gesture-handler is that they have really bad documentation
                disallowInterruption={!panEnabled}
                shouldActivateOnStart={true}
                simultaneousHandlers={scrollView}>
                <Animated.ScrollView
                  onScroll={scroll}
                  ref={scrollView}
                  scrollEventThrottle={16}
                  style={styles.scrollView}>
                  <SafeAreaView>
                    {options.map(({text, id}) => (
                      <ModalItem
                        key={id}
                        text={text}
                        active={activeId === id}
                        onPress={onSelectValue.bind(Modal, id)}
                      />
                    ))}
                    <TouchableOpacity
                      style={[styles.cancelBtn]}
                      onPress={onCancel}>
                      <Text>cancel</Text>
                    </TouchableOpacity>
                  </SafeAreaView>
                </Animated.ScrollView>
              </NativeViewGestureHandler>
            </Animated.View>
          </Animated.View>
        </PanGestureHandler>
      ) : null,
    [
      activeId,
      background,
      handler,
      mainView,
      modalActive,
      onCancel,
      onSelectValue,
      panEnabled,
      scroll,
      maxHeight,
    ],
  );
};

export default Modal;

const styles = StyleSheet.create({
  mainContainer: {
    ...StyleSheet.absoluteFillObject,
    width,
    height,
    backfaceVisibility: 'hidden',
    backgroundColor: 'transparent',
    elevation: 10,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    width,
    height,
    backfaceVisibility: 'hidden',
    backgroundColor: '#000000',
  },

  scrollView: {
    backgroundColor: 'white',
    elevation: 11,
    zIndex: 1,
  },
  scrollViewContainer: {},
  mainView: {
    width,
    position: 'absolute',
    bottom: StatusBar.currentHeight ?? 0,
    left: 0,
    // height: 400,
  },
  pressable: {
    flex: 1,
  },
  cancelBtn: {
    backgroundColor: 'transparent',
    borderTopWidth: 1,
    borderTopColor: '#000',
    paddingTop: 20,
    marginTop: 10,
    paddingLeft: 10,
    paddingVertical: 10,
  },
});
