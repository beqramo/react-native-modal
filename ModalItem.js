import React from 'react';
import {Text, StyleSheet, Pressable} from 'react-native';

const ModalItem = ({text, onPress, active, style}) => {
  return (
    <Pressable
      style={[styles.mainContainer, active ? styles.active : null, style]}
      onPress={onPress}>
      <Text>{text}</Text>
    </Pressable>
  );
};

export default ModalItem;

const styles = StyleSheet.create({
  mainContainer: {
    borderRadius: 10,
    backgroundColor: 'gray',
    padding: 10,
    marginVertical: 5,
    marginHorizontal: 16,
    elevation: 111,
  },
  active: {
    backgroundColor: 'red',
  },
});
