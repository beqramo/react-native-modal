import {View, Button, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import Modal from './Modal';

export default function AnimatedStyleUpdateExample(props) {
  const [isActive, setIsActive] = useState(0);
  const [activeId, setActiveId] = useState(23);

  return (
    <View style={styles.mainContainer}>
      <Button title="toggle" onPress={setIsActive.bind(this, 1)} />
      <Modal
        isActive={isActive}
        onModalToggle={setIsActive}
        activeId={activeId}
        setActiveId={setActiveId}
        maxHeight={300}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
